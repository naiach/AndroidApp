Pour effectuer ce projet nous avons suivi la documentation officielle du site de  NativeScript car nous avons 
peu de compétences en développement mobile et que celle-ci nous paraît très complète et correspond à ce 
qui est attendu pour ce projet, c'est-à-dire l'utilisation d'un framework oriénté MVVM.


La documentation officielle nous a permis de créer une application qui est cross-platform ( Android et IOS),
et qui permet la gestion d'une liste de courses.

Pour cela nous avons importé la structure et le squelette d'application, puis ajouter
une interface utilisateur, et construisent les modèles vues permettant l'inscription via un formulaire et la connexion.

Pour le stockage des informations de connexion,la documentation officielle utilise l'API REST de Telerik.

Nous avons également utilisé une ListView pour permettre d'afficher les différents items de la liste de courses.


L'application telle qu'elle nous permet : -la connexion à une API REST
					  -Permet l'inscription et la connexion
					  - Permet aux personnes authentifié l'ajout et la suppression de listes de courses
					  - Cross Plateforme
					  
Pour lancer l'application(il faut au préalable avoir les outils de nativescript TNS):
tns run android
tns run ios